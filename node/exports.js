// @ts-check

const init = require("../non-browser-common/fetchData.js");
const getData = require("../gadget/tidy-data.js");
const processOnSave = require("../gadget/tidy-on-save.js");

module.exports = {
  init,
  getData,
  process,
  processOnSave,
};

/** @type {getData.Data} */
let data;

/** @type {import("./exports.js").ProcessFn} */
function process(wikitext) {
  data = data || getData();

  const context = processOnSave(wikitext, data);

  return {
    wikitext: context.unopaque(context.wikitext),
    transformationCategories: context.transformCats,
    warnings: context.warnings,
    context,
  };
}
