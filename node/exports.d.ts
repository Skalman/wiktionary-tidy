export const init: typeof import("../non-browser-common/fetchData.js");
export const getData: typeof import("../gadget/tidy-data.js");
export const processOnSave: typeof import("../gadget/tidy-on-save.js");
export const process: ProcessFn;

export type ProcessFn = (wikitext: string) => ProcessResult;

interface ProcessResult {
  wikitext: string;
  transformationCategories: string[];
  warnings: Set<string>;
  context: import("../gadget/tidy-on-save.js").Context;
}
