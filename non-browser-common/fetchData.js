module.exports = fetchData;

async function fetchData() {
  const titles = {
    dataLang: "MediaWiki:Gadget-data-lang.json",
    dataLangReplacements: "MediaWiki:Gadget-data-lang-replacements.json",
    dataLangCodeTemplates: "MediaWiki:Gadget-data-lang-code-templates.json",
    dataH3: "MediaWiki:Gadget-data-h3.json",
    dataHeadings: "MediaWiki:Gadget-data-headings.json",
  };

  const jsonObjects = Object.fromEntries(
    await Promise.all(
      Object.entries(titles).map(async ([variable, title]) => [
        variable,
        await (
          await fetch(
            `https://sv.wiktionary.org/wiki/${title}?action=raw&ctype=application/json`
          )
        ).json(),
      ])
    )
  );

  Object.assign(globalThis, jsonObjects);
}
