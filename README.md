## Tidy

Tidy checks and fixes the wikitext of entries in the Swedish language Wiktionary. It is a MediaWiki gadget. Current deployment found on-wiki: [Special:PrefixIndex/MediaWiki:Gadget-tidy](https://sv.wiktionary.org/wiki/Special:PrefixIndex/MediaWiki:Gadget-tidy).

### Node

Tidy can be used from Node.

```sh
npm install --save git+https://gitlab.com/Skalman/wiktionary-tidy.git

# If using a version of Node that doesn't support fetch()
npm install --save node-fetch@2
```

Example transformation.

```js
// Needed for old versions of Node.
// Use `node-fetch@2` to use with `require()`.
const fetch = require("node-fetch");
globalThis.fetch = fetch;

const { init, process } = require("tidy");

main();

async function main() {
  await init();

  const oldWikitext = "==Svenska==\n{{subst}}";
  const result = process(oldWikitext);

  console.log({
    oldWikitext,
    newWikitext: result.wikitext,
    warnings: result.warnings,
    transformationCategories: result.transformationCategories,
  });
}
```

### License

The code is made available under Mozilla Public License Version 2.0.
