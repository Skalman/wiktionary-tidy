const globals = require("globals");
const { FlatCompat } = require("@eslint/eslintrc");

const compat = new FlatCompat({
  baseDirectory: __dirname,
});

const tsConf = compat.extends("plugin:@typescript-eslint/recommended");
const tsInfra = Object.assign(...tsConf);
const tsRules = Object.assign(...tsConf.map((x) => x.rules));

module.exports = [
  "eslint:recommended",
  {
    files: ["gadget/**.js"],
    languageOptions: {
      ecmaVersion: 5,
      sourceType: "commonjs",
      globals: {
        ...globals.browser,
        ...globals.es2015,
        globalThis: "writable",
      },
    },
  },
  {
    files: ["demo/*.js"],
    languageOptions: {
      globals: {
        ...globals.browser,
      },
    },
  },
  {
    files: ["demo/demo*.js"],
    languageOptions: {
      sourceType: "commonjs",
      globals: {
        ...globals.browser,
      },
    },
  },

  {
    files: ["node/**/*.js", "non-browser-common/**/*.js"],
    languageOptions: {
      sourceType: "commonjs",
      globals: {
        ...globals.node,
      },
    },
  },
  {
    files: ["eslint.config.js"],
    languageOptions: {
      globals: {
        ...globals.node,
      },
    },
  },
  {
    ...tsInfra,
    files: ["**/*.ts"],
    rules: {
      ...tsRules,
    },
  },
];
