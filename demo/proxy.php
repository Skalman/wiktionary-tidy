<?php

// Proxy to be used for running the local version on-wiki.

$f = @$_GET['f'];
if (!$f) $f = './setup-on-wiki.js';

header('Access-Control-Allow-Origin: https://sv.wiktionary.org');

// Allow JS files in this folder and in `../gadget/`.
if (preg_match('#^(\./|\.\./gadget/)[a-z-]+\.js$#', $f)) {
  header('Content-Type: text/javascript; charset=utf-8');
  readfile($f);
} else {
  header('Status: 404');
  echo "Unknown file $f";
  exit;
}
