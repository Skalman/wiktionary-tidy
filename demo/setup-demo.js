// @ts-check

export {};

// Don't await this import; load the navigation in parallel with the rest.
import("./nav.js");

const startupScript = document.createElement("script");
startupScript.src =
  "https://sv.wiktionary.org/w/load.php?lang=sv&modules=startup&only=scripts&raw=1&skin=vector-2022";
document.head.append(startupScript);

// MediaWiki adds script tags that point to the current origin.
// Replace those scripts with ones that point to sv.wiktionary.org.
new MutationObserver((records) => {
  for (const record of records) {
    for (const node of record.addedNodes) {
      if (
        node instanceof HTMLScriptElement &&
        node.src.startsWith(`${location.origin}/w/load.php`)
      ) {
        const copy = document.createElement("script");
        copy.src = node.src.replace(
          location.origin,
          "https://sv.wiktionary.org"
        );
        document.head.append(copy);
      }
    }
  }
}).observe(document.head, { childList: true });

// Wait until MediaWiki has loaded.
for (let i = 1; !(/** @type {*} */ (window).mw?.loader?.using); i *= 2) {
  await new Promise((resolve) => setTimeout(resolve, i));
  if (i > 10_000) {
    throw new Error("Failed to get mw.loader.using");
  }
}

/** @type {import("../gadget/tidy.js").MediaWiki} */
var mw = /** @type {*} */ (window).mw;

/** @type {import("../gadget/tidy.js").JQueryStatic} */
var $ = /** @type {*} */ (window).$;

// Only enabled in the main namespace.
mw.config.set("wgNamespaceNumber", 0);

// Not bot-enabled initially.
mw.config.set("wgUserGroups", ["*"]);

import { addJs } from "./commonjs.js";
import { gadgetFiles, testFile } from "./gadget-files.js";

for (const gadgetFile of gadgetFiles) {
  await addJs(`../gadget/${gadgetFile}`);
}

mw.loader.implement("ext.gadget.tidy", () => {});

const { page } = /** @type {HTMLElement} */ (
  document.querySelector("[data-page]")
).dataset;

switch (page) {
  case "demo": {
    const getFormData = () => {
      const formDataJson = sessionStorage.getItem("tidy:demo:form");
      if (formDataJson) {
        return JSON.parse(formDataJson);
      }

      return {
        wikitext: [
          "==Svenska==",
          "===Substantiv===",
          "{{subst|hoppsan}}",
          "'''ord'''",
          "#definition",
        ]
          .map((x) => `${x}\n`)
          .join(""),
        summary: "",
      };
    };
    const formData = getFormData();
    $("#wpTextbox1").val(formData.wikitext);
    $("#wpSummary").val(formData.summary);

    // Page loaded for action=edit.
    mw.hook("wikipage.editform").fire($("#editform"));

    await addJs("./demo-live.js");
    await addJs("./demo-settings.js");
    break;
  }

  case "demo-submitted": {
    mw.loader.using(["ext.gadget.sessionStorageNotify"]);
    const url = new URL(location.href);
    const { searchParams } = url;
    const wikitext = searchParams.get("wpTextbox1") ?? "";
    const summary = searchParams.get("wpSummary") ?? "";
    if (!wikitext && !summary) {
      location.href = "./";
      break;
    }
    const buttonText =
      searchParams.get("wpSave") ??
      searchParams.get("wpPreview") ??
      searchParams.get("wpDiff");
    $("#wikitext").val(wikitext);
    $("#summary").val(summary);
    $("#button-text").text(buttonText ?? "");
    sessionStorage.setItem(
      "tidy:demo:form",
      JSON.stringify({
        wikitext,
        summary: buttonText === "Publicera ändringar" ? "" : summary,
      })
    );
    break;
  }

  case "test": {
    const pageName = "Wiktionary:Finesser/Tidy/Test";
    const pageNameEncoded = encodeURIComponent(pageName);

    const testPage = await fetch(
      `https://sv.wiktionary.org/api/rest_v1/page/html/${pageNameEncoded}`
    );
    const testPageHtml = await testPage.text();
    const testContent = $("<div>").append($.parseHTML(testPageHtml));
    testContent.find("meta, link, base, title").remove();
    $("body").append(testContent);

    // Page loaded for action=view
    mw.config.set("wgPageName", pageName);
    mw.hook("wikipage.content").fire(testContent);

    await addJs(`../gadget/${testFile}`);
    break;
  }

  case "publish": {
    const trs = [...gadgetFiles, testFile]
      .map((x) => x.replace(".js", ""))
      .sort()
      .map((x) => `${x}.js`)
      .map((filename) => {
        const page = `MediaWiki:Gadget-${filename}`;
        const local = fetch(`../gadget/${filename}`).then((x) => x.text());
        const remote = fetch(
          `https://sv.wiktionary.org/w/rest.php/v1/page/${page}`
        ).then((x) => x.json());

        const textbox = $("<input>", {
          type: "hidden",
          name: "wpTextbox1",
        });
        const submit = $("<input>", {
          type: "submit",
          name: "wpDiff",
          value: "Loading...",
          disabled: "",
        });
        const form = $("<form>", {
          method: "post",
          target: "_blank",
          action: `https://sv.wiktionary.org/wiki/${page}?action=submit`,
          class: "mb-1",
        }).append(
          textbox,
          submit,
          $("<input>", {
            type: "hidden",
            name: "wpUltimateParam",
            value: "1",
          })
        );

        Promise.all([local, remote]).then(([local, remote]) => {
          const remoteSource = (remote.source || "") + "\n";

          if (local === remoteSource) {
            submit.val("Already up to date");
          } else {
            textbox.val(local);

            const diff = local.length - remoteSource.length;
            const diffStr = diff < 0 ? diff : "+" + diff;
            submit.val(`Show diff (${diffStr})`).prop({ disabled: false });
          }
        });

        return $("<tr>").append(
          $("<td>").append(
            $("<a>", {
              text: page,
              href: `https://sv.wiktionary.org/wiki/${page}`,
            })
          ),
          $("<td>").append(form)
        );
      });

    $("#publish-loading").replaceWith($("<table>").append(trs));
    break;
  }

  case "run-on-wiki": {
    const script = location.href.replace(/\/[^/]+$/, "/proxy.php");
    $("#command").text(`importScriptURI("${script}")`);
    break;
  }
}
