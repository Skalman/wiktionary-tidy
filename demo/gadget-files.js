// @ts-check

export const gadgetFiles = [
  "tidy-data.js",
  "tidy-on-focus.js",
  "tidy-on-keydown.js",
  "tidy-on-save.js",
  "tidy-on-save-summary.js",
  "tidy.js",
];

export const testFile = "tidy-test.js";
