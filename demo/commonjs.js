// Fake CommonJS.

const basename = (path) => path.replace(/^.+?\/([^/]+)$/, "$1");

const moduleRegistry = {};

const require = (module) => {
  module = basename(module);
  if (moduleRegistry[module]) {
    return moduleRegistry[module];
  }

  throw new Error(`Module '${module}' not found`);
};

/** @param {string} url */
export async function addJs(url) {
  /** @type {*} */ (window).module = {};
  /** @type {*} */ (window).require = require;
  await import(url);
  moduleRegistry[basename(url)] = /** @type {*} */ (window).module.exports;
}
