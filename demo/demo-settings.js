// @ts-check

/** @type {import("../gadget/tidy.js").MediaWiki} */
var mw = /** @type {*} */ (window).mw;

/** @type {import("../gadget/tidy.js").JQueryStatic} */
var $ = /** @type {*} */ (window).$;

main();

function main() {
  $("#bot").on("click", function () {
    mw.config.set(
      "wgUserGroups",
      $(this).prop("checked") ? ["*", "bot"] : ["*"]
    );
  });

  $("#editform")[0].addEventListener("submit", (e) => {
    const correctBtn =
      e.submitter?.id === "wpPreview" || e.submitter?.id === "wpDiff";
    const enabled = $("#livepreview-setting").prop("checked");

    if (correctBtn && enabled) {
      e.preventDefault();
      $("#livepreview").slideDown();
      setTimeout(() => {
        $("#livepreview").slideUp();
      }, 4000);
    }
  });

  $("#bot, #livepreview-setting")
    .on("click", function () {
      sessionStorage.setItem(
        "tidy:" + $(this).prop("id"),
        "" + $(this).prop("checked")
      );
    })
    .each((_i, elem) => {
      if (sessionStorage.getItem("tidy:" + $(elem).prop("id")) === "true") {
        $(elem).trigger("click");
        $(elem).parents("details").prop({ open: true });
      }
    });
}
