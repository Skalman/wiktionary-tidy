// @ts-check

const corsProxy = "./proxy.php?f=";

load();

async function load() {
  console.log("Loading Tidy...");
  const { addJs } = /** @type {import("./commonjs.js")} */ (
    await import(`${corsProxy}./commonjs.js`)
  );
  const { gadgetFiles } = /** @type {import("./gadget-files.js")} */ (
    await import(`${corsProxy}./gadget-files.js`)
  );

  for (const gadgetFile of gadgetFiles) {
    await addJs(`${corsProxy}../gadget/${gadgetFile}`);
  }
  console.log("Tidy loaded");
}
