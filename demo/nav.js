// @ts-check

export {};

const nav = document.createElement("div");
nav.innerHTML = `
  <label><a href="./">Demo</a></label>
  <label>&bull; <a href="publish.html">Publicera finess</a></label>
  <label>&bull; <a href="test.html">Test</a></label>
  <label>&bull; <a href="run-on-wiki.html">Kör på wikin</a></label>
`;

nav.querySelectorAll("a").forEach((link) => {
  if (link.href === location.href) {
    const b = document.createElement("b");
    b.append(...link.childNodes);
    link.replaceWith(b);
  }
});

document.getElementById("nav")?.append(...nav.childNodes);
