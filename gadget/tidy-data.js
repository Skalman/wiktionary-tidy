// @ts-check

module.exports = getData;

/**
 * @typedef {{
 *  dataLang: {
 *    main: Record<LangCode, LangNameUcfirst>;
 *    onlyEtym: Record<LangCode, LangNameUcfirst>;
 *  };
 *  dataLangCodeTemplates: Record<Template, LangCodeTemplateInfo>;
 *  dataLangReplacements: Record<LangNameLc, ReplacementExplanation>;
 *  dataH3: Record<H3Template, H3HeadingUcfirst>;
 *  dataHeadings: {
 *    h2: ["Källor"];
 *    h4: ["Översättningar"];
 *    h2Exceptions: ExceptionName[];
 *    h3Exceptions: ExceptionName[];
 *    h4Exceptions: ExceptionName[];
 *  }
 * }} DataGlobals
 *
 * @typedef {{
 *  langCodesByUcfirstName: Map<LangNameUcfirst, LangCode>;
 *  langCodesByLcName: Map<LangNameLc, LangCode>;
 *  langCodeTemplates: Map<Template, LangCodeTemplateInfo>;
 *  langReplacements: Map<LangNameLc, ReplacementExplanation>;
 *  h3TemplatesByName: Map<H3HeadingUcfirst, H3Template>;
 *  headings: DataGlobals["dataHeadings"];
 * }} Data
 *
 * @typedef {NString<1>} LangCode E.g. "sv", "en"
 * @typedef {NString<2>} LangNameUcfirst E.g. "Svenska", "Engelska"
 * @typedef {NString<3>} LangNameLc E.g. "svenska", "engelska"
 * @typedef {NString<4>} Template E.g. "adj", "tagg"
 * @typedef {NString<5>} ReplacementExplanation E.g. "''bokmål'' eller ''nynorska''"
 * @typedef {NString<6>} H3Template E.g. "adj", "subst"
 * @typedef {NString<7>} H3HeadingUcfirst E.g. "Adjektiv", "Substantiv"
 * @typedef {NString<8>} ExceptionName
 *
 * @typedef {[min: number, max: number] | "språk"} LangCodeTemplateInfo
 */

/**
 * @template X
 * @typedef {string & { "": X }} NString A "nominally" typed string.
 */

/** @type {Data | undefined} */
var cached;

/** @returns {Data} */
function getData() {
  if (!cached) {
    /** @type {DataGlobals} */
    var w = /** @type {*} */ (globalThis);

    cached = {
      langReplacements: toMap(w.dataLangReplacements, id),
      langCodesByUcfirstName: toMap(w.dataLang.main, flip),
      langCodesByLcName: toMap(w.dataLang.main, flipLangLc),
      langCodeTemplates: toMap(w.dataLangCodeTemplates, id),
      h3TemplatesByName: toMap(w.dataH3, flip),
      headings: w.dataHeadings,
    };
  }

  return cached;

  /**
   * Identity function.
   * @type {<T>(x: T) => T}
   */
  function id(x) {
    return x;
  }

  /**
   * @type {<K, V>(x: [K, V]) => [V, K]}
   */
  function flip(x) {
    return [x[1], x[0]];
  }

  /**
   * Identity function.
   * @type {(x: [LangCode, LangNameUcfirst]) => [LangNameLc, LangCode]}
   */
  function flipLangLc(x) {
    return [/** @type {LangNameLc} */ (x[1].toLowerCase()), x[0]];
  }

  /**
   * @type {(
   *  <KIn extends string, VIn, KOut, VOut>(
   *    obj: Record<KIn, VIn>,
   *    transform: (kv: [KIn, VIn]) => [KOut, VOut]
   *  ) => Map<KOut, VOut>
   * )} */
  function toMap(obj, transform) {
    return new Map(Object.entries(obj).map(/** @type {*} */ (transform)));
  }
}
