// @ts-check

module.exports = processOnFocus;

/** @param {string} wikitext */
function processOnFocus(wikitext) {
  if (!wikitext.includes("{{tidy}}")) {
    return wikitext;
  }

  return wikitext.replace(/{{tidy}}\s*/, "");
}
