import { SaxesParser } from "https://esm.sh/saxes@6.0.0";
import { createRequire } from "https://deno.land/std@0.163.0/node/module.ts";

const require = createRequire(import.meta.url);

const processOnSave: ProcessOnSave = require("../gadget/tidy-on-save.js");
const getData: GetData = require("../gadget/tidy-data.js");
const fetchData: () => Promise<void> = require("../non-browser-common/fetchData.js");

interface Context {
  readonly wikitext: string;
  unopaque(str: string): string;
  warnings: Set<string>;
  transformCats: string[];
}

interface Data {
  "": "";
}

type ProcessOnSave = (wikitext: string, data: Data) => Context;
type GetData = () => Data;

let counter = 0;
type Mode = "warnings" | "change" | "debug";
const mode: Mode = "change";

function processPage({
  ns,
  title,
  text,
}: {
  ns: number;
  title: string;
  text: string;
}) {
  if (ns !== 0) {
    return;
  }

  if (counter % 5000 === 0) {
    console.error(counter, title);
  }

  const context = processOnSave(text, data);
  const { wikitext, transformCats, warnings } = context;
  const newWikitext = context.unopaque(wikitext);

  const changedWikitext = trimEnd(text) !== trimEnd(newWikitext);

  if (mode === "warnings" && warnings.size) {
    const line = title + " # " + Array.from(warnings).join("  ");
    console.log(line);
  } else if (mode === "change" && changedWikitext) {
    const line = title + " # " + transformCats.join(", ");
    console.log(line);
  } else if (mode === "debug") {
    console.log({
      counter,
      title,
      changedWikitext,
      transformCats,
      warnings: Array.from(warnings),
    });
  }
}

function trimEnd(str: string) {
  return str.replace(/\n+$/, "");
}

await fetchData();
const data = getData();

const baseDir = new URL("./", import.meta.url);
const xmlFile = findXmlDump(baseDir);
if (!xmlFile) {
  throw new Error(`Couldn't find dump in ${baseDir}`);
}

console.error(`Using dump ${xmlFile}`);

const sax = new SaxesParser();
const file = await Deno.open(xmlFile, { read: true });
const decoder = new TextDecoderStream();

let page: {
  curTag: "page" | "title" | "text" | "ns" | undefined;
  ns?: string;
  title?: string;
  text?: string;
} = { curTag: undefined };

let prevPage = page;

sax.on("opentag", ({ name }) => {
  switch (name) {
    case "page":
      page = { curTag: "page" };
      break;

    case "ns":
    case "title":
    case "text":
      page.curTag = name;
      break;
  }
});

sax.on("text", (text) => {
  switch (page.curTag) {
    case "ns":
    case "text":
    case "title":
      page[page.curTag] = text;
      break;
  }
});

sax.on("closetag", ({ name }) => {
  switch (name) {
    case "page":
      if (
        page.ns === undefined ||
        page.title === undefined ||
        page.text === undefined
      ) {
        console.error("</page> found, but ns, title or text is missing");
        Deno.exit(1);
      }

      processPage({
        ns: +page.ns,
        title: page.title,
        text: page.text,
      });
      prevPage = page;
      page = { curTag: undefined };

      counter++;

      if (counter > 1_000_000) {
        try {
          sax.close();
        } catch (_e) {
          console.log("sax doc not complete");
        }

        try {
          file.close();
        } catch (_e) {
          // Not sure how to close this in a good way.
        }
      }
      break;

    default:
      page.curTag = undefined;
      break;
  }
});

async function fileToDecoder(decoder: TextDecoderStream, file: Deno.FsFile) {
  const buffer = new Uint8Array(200e3);
  const writer = decoder.writable.getWriter();
  for (;;) {
    const bytes = await file.read(buffer);
    if (bytes === null) {
      console.error("[fileToDecoder] no bytes");
      await writer.abort();
      break;
    }

    if (bytes === 0) {
      console.error("fileToDecoder] Get 0 bytes");
      continue;
    }

    writer.write(bytes === buffer.length ? buffer : buffer.slice(0, bytes));
  }
}

async function decoderToSaxes(decoder: TextDecoderStream, saxes: SaxesParser) {
  const reader = decoder.readable.getReader();
  for (;;) {
    const { done, value } = await reader.read();
    if (done) {
      console.error("[decoderToSaxes] done");
      break;
    }

    try {
      saxes.write(value);
    } catch (e) {
      console.log({ page, prevPage });
      console.log(value);

      throw e;
    }
  }

  console.log("[decoderToSaxes] CLOSE SAXES");
  saxes.close();
}

const start = new Date();
console.error("Start", start);
try {
  await Promise.all([
    fileToDecoder(decoder, file),
    decoderToSaxes(decoder, sax),
  ]);
} catch (e) {
  if (e === undefined) {
    // Do nothing. For whatever reason, `undefined` is thrown from somewhere.
  } else {
    throw e;
  }
}
const end = new Date();
const seconds = (end.getTime() - start.getTime()) / 1000;
console.error(end);
console.error(`DONE in ${seconds} seconds`);

function findXmlDump(baseDir: URL) {
  const filename = [...Deno.readDirSync(baseDir)]
    .map((x) => x.name)
    .filter((x) => /^svwiktionary-\d{8}-pages-articles.xml$/.test(x))
    .sort()
    .at(-1);

  if (filename) {
    return new URL(filename, baseDir);
  }
}
